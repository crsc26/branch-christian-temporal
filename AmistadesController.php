<?php

use Phalcon\Mvc\Controller;

class AmistadesController extends Controller
{
	public function indexAction()
	{
		
	}
	public function verAction($idamistad)
	{
		 $this->view->idamistad= $idamistad;
	}
	public function destruirAction($idamistad)
	{
		$amistad = Amistad::findFirstById($idamistad);
		$idcuate = $amistad->id_cuate;
		$idamigo = $amistad->id_amigo;
		$usercuate = Usuario::findFirstById($idcuate);
		$useramigo = Usuario::findFirstById($idamigo);
		$cuate = Formulariocuate::findFirstByCorreo($usercuate->nombre);
		$amigo = Formularioamigo::findFirstByCorreo($useramigo->nombre);
		$cuate->en_amistad=0;
		$amigo->en_amistad=0;
		$cuate->save();
		$amigo->save();
		$amistad->delete();
	}
	public function comentarioAction($idamistad)
	{

		$comentario = new Comentario();
		
		$comentario->idamistad=$idamistad;

		if($this->session->has("ADMIN"))
		{
			$id = $this->session->get("ADMIN");
			$comentario->nombre = "Administrador";
			$comentario->id_comentador = $id;
		}
		else if($this->session->has("AMIGO"))
		{
			$id = $this->session->get("AMIGO");
			$usuario = Usuario::findFirstById($id);
			$amigo = Formularioamigo::findFirstByCorreo($usuario->nombre);
			$comentario->nombre=$amigo->nombre;
			$comentario->id_comentador = $id;
		}
		else if($this->session->has("CUATE"))
		{
			$id = $this->session->get("CUATE");
			$usuario = Usuario::findFirstById($id);
			$cuate = Formulariocuate::findFirstByCorreo($usuario->nombre);
			$comentario->nombre=$cuate->nombre;
			$comentario->id_comentador = $id;
		}
		$comentario->comentario=$this->request->getPost("comentario");

		$comentario->save();
		$this->response->redirect("Amistades/ver/$idamistad");
		
	}
}

