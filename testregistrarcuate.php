<?php

namespace registrarcuate;

/**
 * Class UnitTest
 */

// Unit Test

class UnitTest extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formulariocuate();
        $nombre = $this->request->getPost("nombre");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($forma->save(), 1, "Guardado"))
        {
            echo "Funciona";
        }
    }
}

class UnitTest2 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("nombre");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($nombre->getPost("")))
        {
            echo "Falta nombre";
        }
    }
}

class UnitTest3 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($edad->getPost("")))
        {
            echo "Falta edad";
        }
    }
}

class UnitTest4 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($sexo->getPost("")))
        {
            echo "Falta sexo";
        }
    }
}

class UnitTest5 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($calle->getPost("")))
        {
            echo "Falta calle";
        }
    }
}

class UnitTest6 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($numero->getPost("")))
        {
            echo "Falta numero";
        }
    }
}

class UnitTest7 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($colonia->getPost("")))
        {
            echo "Falta colonia";
        }
    }
}

class UnitTest7 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($municipio->getPost("")))
        {
            echo "Falta municipio";
        }
    }
}

class UnitTest8 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($codigo_postal->getPost("")))
        {
            echo "Falta codigo postal";
        }
    }
}

class UnitTest9 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($estado->getPost("")))
        {
            echo "Falta estado";
        }
    }
}

class UnitTest10 extends UnitTestCase
{
    public function testTestCase()
    {
        $forma = new Formularioamigo();
        $nombre = $this->request->getPost("");
        $edad = $this->request->getPost("edad");
        $sexo = $this->request->getPost("sexo");
        $calle = $this->request->getPost("calle");
        $numero = $this->request->getPost("numero");
        $colonia = $this->request->getPost("colonia");
        $municipio = $this->request->getPost("delegacion");
        $codigo_postal = $this->request->getPost("codigo_postal");           
        $estado = $this->request->getPost("estado");                        
        $telefono_casa = $this->request->getPost("telefono_casa");  
        $celular = $this->request->getPost("celular");  
        $correo = $this->request->getPost("correo");                       
        $telefono_contacto_emergencia = $this->request->getPost("telefono_contacto_emergencia");  
        $nombre_escuela = $this->request->getPost("nombre_escuela");               
        $year_semestre = $this->request->getPost("year_semestre");                
        $generacion = $this->request->getPost("generacion");                   
        $matricula = $this->request->getPost("matricula");                    
        $carrera = $this->request->getPost("carrera");                      
                         
        $amistad_es = $this->request->getPost("amistad_es");                   
        $cual_sido = $this->request->getPost("cual_sido");                    
                           
        $carro = $this->request->getPost("carro");                        
        $fecha_expiracion = $this->request->getPost("fecha_expiracion");             
        $lo_usaras = $this->request->getPost("lo_usaras");                    
        $fotografiado = $this->request->getPost("fotografiado");                 
        $expulsado = $this->request->getPost("expulsado");                    
        $crimen = $this->request->getPost("crimen");                       
        $firma = $this->request->getPost("firma");                        

        //$success = $user->save($this->request->getPost(), 'nombre');
        
        $forma->nombre = $nombre;
        $forma->sexo = $sexo;
        $forma->edad = $edad;
        $forma->fecha_nacimiento = $fecha_nacimiento;
        $forma->calle = $calle;
        $forma->numero = $numero;
        $forma->colonia = $colonia;
        $forma->delegacion_municipio = $municipio;
        $forma->codigo_postal = $codigo_postal;
        $forma->estado = $estado;
        $forma->telefono_casa = $telefono_casa;
        $forma->celular = $celular;
        $forma->correo = $correo;
        $forma->telefono_contacto_emergencia = $telefono_contacto_emergencia;
        $forma->nombre_escuela = $nombre_escuela;
        $forma->year_semestre = $year_semestre;
        $forma->generacion = $generacion;
        $forma->matricula = $matricula;
        $forma->carrera = $carrera;
        
        $forma->amistad_es = $amistad_es;
        $forma->cual_sido = $cual_sido;

        $forma->carro = $carro;
        $forma->fecha_expiracion = $fecha_expiracion;
        $forma->lo_usaras = $lo_usaras;
        $forma->fotografiado = $fotografiado;
        $forma->expulsado = $expulsado;
        $forma->crimen = $crimen;
        $forma->firma = $firma;
        $forma->fecha = date("yyyy-mm-dd");
        
        if($this->assertEquals($telefono_casa->getPost("")))
        {
            echo "Falta telefono de casa";
        }
    }
}
?>