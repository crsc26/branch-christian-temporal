<?php

	$messages = array(
	//index
	"usuario" => "Usuario",
	"contraseña" => "Contraseña",
	"entrar" => "Entrar",
	"registrar" => "Registrate Ahora!",
	"olvidar" => "Olvidaste tu contraseña?",
	"resgistrar_a" => "Registrar Amigo",
	"registrar_c" => "Registrar Cuate",
	"cerrar" => "Cerrar",
	//admin_main
	"ver_amigos" => "Ver Amigos",
	"ver_cuates" => "Ver Cuates",
	"ver_perfil" => "Ver Perfil",
	"crear_amsitades" => "Crear Amistades",
	"ver_amistades" => "Ver Amistades",
	"configuraciones" => "Configuraciones",
	"salir" => "Salir",
	"asignar_a" => "Asignar amistad a Amigo",
	"asignar_c" => "Asignar amistad a Cuate",
	//admin/veramigos, admin/vercuates
	"amigos_r" => "Amigos Registrados",
	"cuates_r" => "Cuates Registrados",
	///Crearamistad/amistades
	"amistades" => "Amistades",
	//amigo-cuate main
	"perfil_cuate" => "Perfil Cuate",
	"perfil_cuate" => "Perfil Amigo",
	"chat" => "Chat",
	"foto_album" => "Foto Album",
	"comentarios" => "Comentarios",
	"perfil_amsitad" => "Perfil Amistad",
	//perfiles 
	"sexo" => "Sexo: ",
	"edad" => "Edad: ",
	"fecha" => "Fecha de nacimiento: ",
	"correo" => "Correo: ",
	"padre" => "Padre: ",
	"madre" => "Madre: ",
	"horario" => "Horario de llamada: ",
	"casa" => "Casa: ",
	"celular" => "Celular: ",
	"oficina" => "Oficina: ",
	"llamar" => "Llamar al telefono: ",
	"acordar" => "Para acordar lugares y horarios de salida: ",
	"codigo" => "Codigo Postal: ",
	"estudia_en" => "Estudia en: ",
	"semestre" => "Semestre: ",
	"matricula" => "Matricula",
	"emergencia" => "Emergencia",

	//chat
	"enviar_c" => "Enviar"
        
    // Vista Admin

    "Amigos" => "Amigos",
    "Cuates" => "Cuates",
    "cuate" => "cuate",
        
    // Vista Amigo
        
    "amigo" => "amigo",
        
    // Vista Amistades index
        
    "Registrados" => "Registrados",
    "Crear" => "Crear",
        
    // Vista Amistades ver
        
    "Amistad" => "Amistad",
    "idamistad" => "Id Amistad",
    "tamano" => "tamano",
    "indice" => "indice",
    "Comentario" => "Comentario",
    "nombre" => "nombre",
    "enviar" => "enviar",
        
    // Vista Chat admin
        
    "id" => "id",
        
    // Vista Chat amigocuate
        
    "id_amigo" => "Id Amigo",
    "mensaje" => "mensaje",
    "enviarmensaje" => "enviar mensaje"
        
    // Vista Foto index
        
    "Foto" => "Foto",
        
    // Vista Index index
        
    "Usuario" => "Usuario",
    
    // Vista Modificar amigo
        
    "Completo" => "Completo",
    "hombre" => "hombre",
    "mujer" => "mujer",
    "fecha_nacimiento" => "Fecha de Nacimiento",
    "Dirección" => "Dirección",
    "calle" => "calle",
    "Número" => "Número",
    "Colonia" => "Colonia",
    "delegacion_municipio" => "delegación o municipio",
    "Estado" => "Estado",
    "estado" => "estado",
    "Telefono" => "Telefono",
    "amigo_telefono" => "Telefono de Amigo",
    "email" => "email",
    "telefono_emergencia" => "Teléfono y Contacto en caso de emergencia",
    "nombre_escuela" => "Nombre de la escuela",
    "generacion" => "generacion",
    "carrera" => "carrera",
    "autorizacion" => "Doy autorización para ser fotografiado o filmado en cualquier actividad relacionada con Best Buddies de México y entiendo que las fotografías o videos serán utilizados bajo la discreción de Best Buddies con fines publicitarios",
    "expulsado" => "¿Has sido expulsado, despedido o te han pedido renunciar de algún trabajo pagado o voluntario a causa de algún acoso sexual?",
    "acusado" => "¿Has sido penalmente acusado de algún crimen sexual, incluyendo acoso o abuso sexual a menores?",
    "Modificar" => "Modificar",
        
    // Vista Modificar cuate
        
    "Formulario" => "Formulario"
    "nombre_padre" => "Nombre del Padre",
    "nombre_madre" => "Nombre de la Madre",
    "personalidad" => "Por favor describe tu personalidad",
    "mensaje_necesidad" => "Menciona cualquier necesidad especial de la cual Best Buddies deba estar informado: (enfermedades, alergías, uso de bloqueador, medicamentos, etc.)",
    "gustaria_visitar" => "¿Qué te gustaría hacer con tu nuevo amigo?",
    "gracias" => "Gracias",
    "enviar_formato" => "Enviar Formato",
        
    // Vista Olvidar index
        
    "recuperar_cuenta" => "Recuperar Cuenta",
        
    // Vista Olvidar index
        
    "de_nuevo" => "Escribe de nuevo tu contraseña",
    "poner" => "Poner nueva contraseña",
        
    // Vista Olvidar olvidar
        
    "revisa" => "Listo! Por favor revisa tu email en unos minutos.",
    "regresar" => "Regresar a Inicio",
        
    // Vista User amigo
            
    "Bienvenido" => "Bienvenido",
    "breve" => "Breve explicacion",
    
    // Controlador AdminController
        
    "logout" => "logout",
    "nombre_cuate" "Nombre Cuate",
    "eliminar" => "eliminar",
        
    // Controlador AmistadesController
        
    "Administrador" => "Administrador",
        
    // Controlador RegistrarController
        
    "videojuego" => "videojuego",
        
	);

?>