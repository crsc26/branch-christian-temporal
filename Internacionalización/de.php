<?php

	$messages = array(
	//index
	"usuario" => "Benutzer",
	"contraseña" => "Passwort",
	"entrar" => "Anmeldung",
	"registrar" => "Jetzt registrieren!!",
	"olvidar" => "Passwort vergessen??",
	"resgistrar_a" => "registrieren Amigo",
	"registrar_c" => "registrieren Cuate",
	"cerrar" => "schliessen",
	//admin_main
	"ver_amigos" => "Sehen Amigos",
	"ver_cuates" => "Sehen Cuates",
	"ver_perfil" => "Profil",
	"crear_amsitades" => "schaffen Freundschaften",
	"ver_amistades" => "Sehen Amistades",
	"configuraciones" => "Konfigurationen",
	"salir" => "Ausloggen",
	"asignar_a" => "Vergeben Freundschaft Amigo",
	"asignar_c" => "Vergeben Freundschaft Cuate",
	//admin/veramigos, admin/vercuates
	"amigos_r" => "Registrierte Amigos",
	"cuates_r" => "Registrierte Cuates",
	///Crearamistad/amistades
	"amistades" => "Freundschaft",
	//amigo-cuate main
	"perfil_cuate" => "Profil Cuate",
	"perfil_cuate" => "Profil Amigo",
	"chat" => "Chat",
	"foto_album" => "Foto Album",
	"comentarios" => "Kommentare",
	"perfil_amsitad" => "Profil Amistad",
	//perfiles 
	"sexo" => "Sex: ",
	"edad" => "Alter: ",
	"fecha" => "Geburtsdatum: ",
	"correo" => "Mail: ",
	"padre" => "Vater: ",
	"madre" => "Mutter: ",
	"horario" => "Anrufzeiten: ",
	"casa" => "Haus: ",
	"celular" => "Cell: ",
	"oficina" => "Büro: ",
	"llamar" => "Telefonat: ",
	"acordar" => "So ordnen Sie Orte und Abfahrtszeiten: ",
	"codigo" => "Postleitzahl: ",
	"estudia_en" => "Studieren in: ",
	"semestre" => "Semester: ",
	"matricula" => "Immatrikulation",
	"emergencia" => "Notfall",

	//chat
	"enviar_c" => "senden"
        
    // Vista Admin

    "Amigos" => "Amigos",
    "Cuates" => "Cuates",
    "cuate" => "cuate",
        
    // Vista Amigo
        
    "amigo" => "amigo",
        
    // Vista Amistades index
        
    "Registrados" => "Eingetragen",
    "Crear" => "Schaffen",
        
    // Vista Amistades ver
        
    "Amistad" => "Freundschaft",
    "idamistad" => "ID Freundschaft",
    "tamano" => "Größe",
    "indice" => "index",
    "Comentario" => "Kommentar",
    "nombre" => "name",
    "enviar" => "senden",
        
    // Vista Chat admin
        
    "id" => "id",
        
    // Vista Chat amigocuate
        
    "id_amigo" => "ID Freund",
    "mensaje" => "nachricht",
    "enviarmensaje" => "Nachricht senden"
        
    // Vista Foto index
        
    "Foto" => "Foto",
        
    // Vista Index index
        
    "Usuario" => "Benutzer",
    
    // Vista Modificar amigo
        
    "Completo" => "Benutzer",
    "hombre" => "Mann",
    "mujer" => "Frau",
    "fecha_nacimiento" => "Geburtsdatum",
    "Dirección" => "Adresse",
    "calle" => "Straße",
    "Número" => "Anzahl",
    "Colonia" => "Köln",
    "delegacion_municipio" => "Delegation",
    "Estado" => "Staat",
    "estado" => "staat",
    "Telefono" => "Telefon",
    "amigo_telefono" => "Telefon Freund",
    "email" => "e-mail",
    "telefono_emergencia" => "Telefon und Notruf",
    "nombre_escuela" => "Name der Schule",
    "generacion" => "menschenalter",
    "carrera" => "beruf",
    "autorizacion" => "Ich gebe die Erlaubnis, eine Tätigkeit fotografiert oder gefilmt werden im Zusammenhang mit Best Buddies Mexico und zu verstehen, dass Fotos oder Videos wird nach Ermessen von Best Buddies zu Werbezwecken verwendet werden",
    "expulsado" => "Haben Sie vertrieben, gefeuert oder Sie haben Verzicht auf eine bezahlte oder freiwillige Arbeit wegen einer sexuellen Belästigung gefordert hat?",
    "acusado" => "Haben Sie schon mit einem sexuellen Verbrechen, einschließlich sexueller Belästigung oder Kindesmissbrauch kriminell belastet?",
    "Modificar" => "Abändern",
        
    // Vista Modificar cuate
        
    "Formulario" => "Form"
    "nombre_padre" => "Name des Vaters:",
    "nombre_madre" => "Name der Mutter",
    "personalidad" => "Bitte beschreiben Sie Ihre Persönlichkeit",
    "mensaje_necesidad" => "Erwähnen Sie besondere Wünsche, die Best Buddies müssen gemeldet werden: (Krankheiten, Allergien, Verwendung von Sonnenschutz , Medikamente usw.)",
    "gustaria_visitar" => "Was möchten Sie mit Ihrem neuen Freund zu tun?",
    "gracias" => "dank",
    "enviar_formato" => "senden format",
        
    // Vista Olvidar index
        
    "recuperar_cuenta" => "Verlorene Konto",
        
    // Vista Olvidar index
        
    "de_nuevo" => "Umschreiben Sie Ihr Passwort",
    "poner" => "Setzen Sie neues Passwort",
        
    // Vista Olvidar olvidar
        
    "revisa" => "Erledigt!. Bitte überprüfen Sie Ihre E-Mail in wenigen Minuten",
    "regresar" => "Rückkehr",
        
    // Vista User amigo
            
    "Bienvenido" => "Willkommen",
    "breve" => "kurze Erklärung",
    
    // Controlador AdminController
        
    "logout" => "logout",
    "nombre_cuate" "Name des Cuate",
    "eliminar" => "entfernen",
        
    // Controlador AmistadesController
        
    "Administrador" => "Verwalter",
        
    // Controlador RegistrarController
        
    "videojuego" => "Videospiel",

	);

?>