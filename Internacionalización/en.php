<?php

	$messages = array(
	//index
	"usuario" => "User",
	"contraseña" => "Password",
	"entrar" => "Login",
	"registrar" => "Register Now!",
	"olvidar" => "Forgot your Password?",
	"resgistrar_a" => "Register Amigo",
	"registrar_c" => "Register Cuate",
	"cerrar" => "Close",
	//admin_main
	"ver_amigos" => "See Amigos",
	"ver_cuates" => "See Cuates",
	"ver_perfil" => "See Profile",
	"crear_amsitades" => "Create Friendship",
	"ver_amistades" => "See Friendships",
	"configuraciones" => "Configurations",
	"salir" => "Logout",
	"asignar_a" => "Assign friendship to Amigo",
	"asignar_c" => "Assign friendship to cuate",
	//admin/veramigos, admin/vercuates
	"amigos_r" => "Registered Amigos",
	"cuates_r" => "Registered Cuates",
	///Crearamistad/amistades
	"amistades" => "Friendships",
	//amigo-cuate main
	"perfil_cuate" => "Cuate Profile",
	"perfil_cuate" => "Amigo Profile",
	"chat" => "Chat",
	"foto_album" => "Photo Album",
	"comentarios" => "Comments",
	"perfil_amsitad" => "Friendship Profile",
	//perfiles 
	"sexo" => "Sex: ",
	"edad" => "Age: ",
	"fecha" => "Date of Birth: ",
	"correo" => "Mail: ",
	"padre" => "Father: ",
	"madre" => "Mother: ",
	"horario" => "Call schedule: ",
	"casa" => "Home: ",
	"celular" => "Cellphone: ",
	"oficina" => "Office: ",
	"llamar" => "Call phone: ",
	"acordar" => "To agree on place and time of outing: ",
	"codigo" => "Postal code: ",
	"estudia_en" => "Goes to: ",
	"semestre" => "Semester: ",
	"matricula" => "School ID",
	"emergencia" => "Emergency",
	//chat
	"enviar_c" => "Send"
        
    // Vista Admin

    "Amigos" => "Amigos",
    "Cuates" => "Cuates",
    "cuate" => "buddie",
        
    // Vista Amigo
        
    "amigo" => "amigo",
        
    // Vista Amistades index
        
    "Registrados" => "Registered",
    "Crear" => "Create",
        
    // Vista Amistades ver
        
    "Amistad" => "Friendship",
    "idamistad" => "Friendship ID",
    "tamano" => "size",
    "indice" => "index",
    "Comentario" => "comment",
    "nombre" => "name",
    "enviar" => "send",
        
    // Vista Chat admin
        
    "id" => "id",
        
    // Vista Chat amigocuate
        
    "id_amigo" => "Friend ID",
    "mensaje" => "message",
    "enviarmensaje" => "Send Message"
        
    // Vista Foto index
        
    "Foto" => "Photo",
        
    // Vista Index index
        
    "Usuario" => "User",
    
    // Vista Modificar amigo
        
    "Completo" => "Complete",
    "hombre" => "man",
    "mujer" => "woman",
    "fecha_nacimiento" => "Birthdate",
    "Dirección" => "Address",
    "calle" => "Street",
    "Número" => "Number",
    "Colonia" => "Neighborhood",
    "delegacion_municipio" => "city or county",
    "Estado" => "State",
    "estado" => "state",
    "Telefono" => "Telephone",
    "amigo_telefono" => "Friend telephone",
    "email" => "email",
    "telefono_emergencia" => "Emergency and Telephone Contact",
    "nombre_escuela" => "School Name",
    "generacion" => "Class of",
    "carrera" => "Major",
    "autorizacion" => "I give permission to be photographed or filmed in any activity related to Best Buddies Mexico and understand that photographs or videos will be used at the discretion of Best Buddies for advertising purposes",
    "expulsado" => "Have you been expelled, fired or you have requested waiver of any paid or volunteer work because of some sexual harassment?",
    "acusado" => "Have you been criminally charged with a sexual crime, including sexual harassment or child abuse?",
    "Modificar" => "Modify",
        
    // Vista Modificar cuate
        
    "Formulario" => "Form"
    "nombre_padre" => "Name of your father",
    "nombre_madre" => "Name of your mother",
    "personalidad" => "Please describe your personality",
    "mensaje_necesidad" => "Mention any special needs which Best Buddies must be reported: (diseases, allergies, use of sunscreen, medicines, etc.)",
    "gustaria_visitar" => "What would you like to do with your new friend?",
    "gracias" => "Thanks",
    "enviar_formato" => "Send Form",
        
    // Vista Olvidar index
        
    "recuperar_cuenta" => "Recover account",
        
    // Vista Olvidar index
        
    "de_nuevo" => "Please write your password again",
    "poner" => "Write new password",
        
    // Vista Olvidar olvidar
        
    "revisa" => "Done!. Please check your email in a few minutes",
    "regresar" => "Come back to Index",
        
    // Vista User amigo
            
    "Bienvenido" => "Welcome",
    "breve" => "Short Explanation",
    
    // Controlador AdminController
        
    "logout" => "logout",
    "nombre_cuate" "Cuate name",
    "eliminar" => "delete",
        
    // Controlador AmistadesController
        
    "Administrador" => "Administrator",
        
    // Controlador RegistrarController
        
    "videojuego" => "Videogame",

	);

?>