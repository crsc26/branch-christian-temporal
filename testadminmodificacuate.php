<?php

class ExampleTest extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av Delmal'],
         ['Colonia' => 'Col Inferno'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'x');
        }
       
    }
    
    class ExampleTest2 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

     // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 321'],
         ['Colonia' => 'Col 321'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_index');
        }
       
    }
    
    
    class ExampleTest3 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '78220'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8vo'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_registrar_cuate');
        }
       
    }
    
    class ExampleTest4 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

     // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => ''],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '6t0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_nombre');
        }
       
    }
    
    class ExampleTest4 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8vo'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'No_edad');
        }
       
    }
    
    
    class ExampleTest5 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

     // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => ''],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '6t0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'No_sexo');
        }
       
    }
    
    class ExampleTest6 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => ''],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_calle');
        }
       
    }
    
    class ExampleTest7 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => ''],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8vo'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'No_colonia');
        }
       
    }
    
    class ExampleTest8 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => ''],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'No_codigo');
        }
       
    }
    
    class ExampleTest10 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => ''],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8vo'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_estado');
        }
       
    }
    
    class ExampleTest11 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => ''],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_tel_casa');
        }
       
    }
    
    class ExampleTest11 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => ''],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'No_cel');
        
        }
       
    }
    
    class ExampleTest13 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => ''],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_mail');
        }
       
    }
    
    
    class ExampleTest15 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => ''],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_escuela');
        }
       
    }
    
    class ExampleTest17 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => ''],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_generacion');
        }
       
    }
    
    
    class ExampleTest18 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate
')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => ''],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'NO_carrera');
        }
       
    }
    
    class ExampleTest19 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => ''],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_fotografiado');
        }
       
    }
    
    class ExampleTest21 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8vo'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => ''],
         ['crimen' => 'no']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_expulsado');
        }
       
    }
    class ExampleTest22 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Formato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => '']
         )
         
         ->andSee('Inicio')
         ->onPage('http://162.243.38.244/proyecto/Index');
   
    // Test 4
    function it_verifies_information_in_the_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_crimen');
        }
       
    }
    
    class ExampleTest23 extends IntegrationTest
{

    // Test 1
    function it_verifies_that_pages_load_properly()
    {
        $this->visit('http://162.243.38.244/proyecto/Index');
    }

    // Test 2
    function it_follows_links()
    {
        $this->visit('/http://162.243.38.244/proyecto/Registrar/cuate')
             ->click('Registrate Ahora!')
             ->click('Registrar Cuate')
             ->andSee('Fomato Del Cuate')
             ->onPage('/http://162.243.38.244/proyecto/Registrar/cuate');
    }

    // Test 3
    function it_submits_forms()
   
        $this->visit(('/http://162.243.38.244/proyecto/Registrar/cuate')
         ->submitForm('Submit', 
         ['nombre' => 'Christian'],
         ['edad' => '22']
         ['Sexo' => 'Masculino'],
         ['Calle' => 'Av 666'],
         ['Colonia' => 'Col 666'],
         ['Delegacion' => 'Del 666'],
         ['Codigo_Postal' => '58270'],
         ['Estado' => 'Qro'],
         ['Telefono_casa' => '1234567890'],
         ['Celular' => '0987654321'],
         ['correo' => 'ejemplo123@123.com'],
         ['nombre_escuela' => 'ITESM'],
         ['semestre' => '8v0'],
         ['generacion' => '2016'],
         ['matricula' => 'a01063685'],
         ['Carrera' => '2016'],
         ['fotografiado' => 'no'],
         ['expulsado' => 'no'],
         ['crimen' => 'no']
         )
         
         ->andSee('')
         ->onPage('');
   
    // Test 4
    function it_verifies_information_in_te_database()
    {
        if($forma->save())
        {
            $frase = "si";
        }
        else 
        {
            $frase = "no";
        }
        $this-> assertEquals($frase, 'si', 'no_final redirect');
        }
       
    }
    
    
?>